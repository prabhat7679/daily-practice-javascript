const onlineStore = {
  storeName: "TechMart",
  location: "New York, USA",
  categories: [
    {
      categoryName: "Electronics",
      products: [
        {
          productName: "Smartphone",
          price: 799.99,
          reviews: [
            { customer: "John", rating: 4.5, comment: "Great phone!" },
            { customer: "Jane", rating: 5, comment: "Best smartphone I've ever had!" },
          ],
        },
        {
          productName: "Laptop",
          price: 1299.99,
          reviews: [
            { customer: "Mike", rating: 4, comment: "Fast and powerful." },
            { customer: "Emily", rating: 4.8, comment: "Excellent for work and gaming." },
          ],
        },
      ],
    },
    {
      categoryName: "Home Appliances",
      products: [
        {
          productName: "Smart TV",
          price: 1499.99,
          reviews: [
            { customer: "Sophia", rating: 4.2, comment: "Great picture quality." },
            { customer: "William", rating: 3.5, comment: "Not user-friendly." },
          ],
        },
      ],
    },
  ],
};
     
  
    // Can you find the customer who gave the highest rating to the "Smart TV"?
    // Calculate the average rating for the "Smartphone" reviews.
    // Is there any product with no reviews? If so, list their names.
    // Create a new array with all the product names sorted alphabetically.
    // Write a function to calculate the total revenue generated from all products in the store.
    // Find the category with the highest average product price.
    // How many customers have left reviews for products in the "Electronics" category?

    // 1.How many categories are there in the online store?
const numberOfCatagories=onlineStore.categories.reduce((acc, curr)=>{
    acc+=1;
    return acc;
}, 0)
console.log(numberOfCatagories)

 // 2.What is the price of the "Laptop" in the "Electronics" category?
const  priceLaptop= onlineStore.categories.reduce((acc, curr)=>{
   if(curr.categoryName=== "Electronics")
   {
    
    const ans2= curr.products.reduce((acc, currr)=>{
        if(currr.productName==="Laptop")
        {
            acc[currr.productName]=currr.price
            // console.log(currr.price)
        }
        return acc;
        
    },{})
//    console.log(ans2)
    acc= ans2;
   }
   return acc;
},{})
console.log( priceLaptop)

  // 3.How many products have a rating above 4.5?
  const ratingAbove = onlineStore.categories.reduce((acc, curr)=>{
   
    const productsAboveRating = curr.products.filter((product) => {
        return product.reviews.some((review) => {
            return review.rating > 4.5
        });
      });
    
      return acc + productsAboveRating.length;
  },0)
  console.log(ratingAbove)